# screen item(path,transition=None):

#     $bgsize=(942,668)

#     $thewidth = renpy.image_size(path)[0]+100
#     $theheight = renpy.image_size(path)[1]+100

#     style_prefix "item"

#     frame at show_hide_move:
        

#         add Crop( ( bgsize[0]/2 - thewidth/2 , bgsize[1]/2 - theheight/2 ,
#                             thewidth-10, theheight-10),"gui/art/itembg.png"):
#             xalign 0.5
#             yalign 0.5
#         background Frame("gui/art/itembgframe.png", 5,5, tile=False)
        
#         add path yalign 0.5 xalign 0.5
        
#         xsize thewidth
#         ysize theheight
#         xalign 0.5
#         yalign 0.5
#         yoffset -100
#         padding (200,200)

transform show_hide_move:
    subpixel True
    on show:
        xoffset 200 alpha 0.0
        easein_back 0.3 xoffset 0 alpha 1.0
    on hide:
        
        xoffset 0 alpha 1.0 zoom 1.0 yoffset 0
        pause 0.0
        ease_quint 0.7 xoffset 300 alpha 1 zoom 0.5 yoffset 850

transform grow_bg:
    on show:
        zoom 0.5 
        pause 0.1
        easein_back 0.2 zoom 1.0 alpha 1.0 
    on hide:
        zoom 1.0 alpha 1.0 yoffset 0
        pause 0
        ease_quad 0.5 zoom 0.5 alpha 0.0 yoffset 500


screen item(path,transition=None):

    

    style_prefix "item"
    zorder 500            
    add path yalign 0.5 xalign 0.5 yoffset -100 at show_hide_move
    #use framebackground
    #$renpy.show_screen("framebackground")

screen framebg:

   
    zorder -1
    add "images/art/focalshadow.png" at grow_bg:

        yoffset -100
        yalign 0.5
        xalign 0.5

# init python:
#     def showitem(path):
#         renpy.show_screen("newitemscreen",path)

#     def hideitem():
#         renpy.hide_screen("newitemscreen")