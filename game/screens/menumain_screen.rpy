

## _custom
screen tab(tabName="TABNAME", isCurrentMenu=False, myAction=Function):

#https://www.renpy.org/doc/html/screen_actions.html#SensitiveIf

    #button action myAction("menumain",None,tabName): #Show always sets sensitive to none for some reason
    button action myAction(renpy.show_screen,"menumain",tabName): #Show always sets sensitive to none for some reason
        text tabName.capitalize()

        
        if isCurrentMenu == True:
            style_prefix "highlighted_tab"

        if isCurrentMenu == False:
            style_prefix "tab"   
            at tab
                

transform tab:
    on hover:
        easein_back 0.1 yoffset 10
    on idle:
        ease 0.06 yoffset 0


    

screen menu_tabs(currentMenu=""):

    button style "return_button" style_prefix "return_button" action Return()

    tag tabs
    style_prefix "menu_tabs"

    $save = "save"
    $load = "load"
    $settings = "settings"
    $quit = "quit"

    hbox:

        use tab(save, (currentMenu==save))
        use tab(load, (currentMenu==load))
        use tab(settings, (currentMenu==settings))
        use tab(quit, (currentMenu==quit), lambda *args: Quit(None))
        


init python:

    class ourOptions:

        #TODO: initialise selectedindex from options

        def __init__(self,key_list=[],desc_list=[]):
            self.key_list = key_list
            self.desc_list = desc_list
            self.selectedIndex = 0
            self.length = len(key_list)

            if len(self.key_list) != len(self.desc_list):
                 raise Exception("key list and description list are different lengths")

        def get_value_at_key(self,targetkey):
            for index, value in enumerate(self.key_list):
                if self.key_list[index] == targetkey:
                    return self.desc_list[index]

        def set_current(self):
            print "oop"

        def __getitem__(self,key):
            return (self.key_list[key],self.desc_list[key])
            print key

        def __len__(self):
            return len(self.key_list)

        def select(index):
            self.selectedIndex = index

       

    viewPortOptionsPixelWidth = 532

    pronoun_options = ourOptions(["he","she","they"],["he/him/his","she/her/hers","they/them/theirs"])
    cps_options = ourOptions(["slow","med","inst"],["Slow (default)","Fast","Instant"])
    font_options = ourOptions(["default","opendyslexic"],["Default","OpenDyslexic"])
    selfvoice_options = ourOptions(["disabled","enabled"],["Disabled (default)","Enabled"])

    pronounadjust = ui.adjustment(range=pronoun_options.length,value=0,step=1)
    cpsadjust = ui.adjustment(range=cps_options.length,value=0,step=1)
    fontadjust = ui.adjustment(range=font_options.length,value=0,step=1)
    selfvoiceadjust = ui.adjustment(range=selfvoice_options,value=0,step=1)

    class setViewportBox(Action):

        def __init__(self, direction="", options=None, adjust=None, prefKey=None):
            
            self.direction = direction
            self.options = options
            self.adjust = adjust
            self.prefKey = prefKey
            

        def __call__(self):

            if self.direction == "right":

                if (self.options.selectedIndex < self.options.length - 1):
                    self.options.selectedIndex = self.options.selectedIndex+1
                    targetScroll = self.options.selectedIndex*viewPortOptionsPixelWidth
                    self.adjust.change(targetScroll)
                    renpy.restart_interaction()

            if self.direction == "left":

                if (self.options.selectedIndex > 0):
                    self.options.selectedIndex = self.options.selectedIndex-1
                    targetScroll = self.options.selectedIndex*viewPortOptionsPixelWidth
                    self.adjust.change(targetScroll)
                    renpy.restart_interaction()

        def get_sensitive(self):
            if self.direction == "left":
                if self.options.selectedIndex > 0:
                    return True
                else:
                    return False


            
            if self.direction == "right":
                if self.options.selectedIndex < self.options.length - 1:
                    return True
                else:
                    return False

    #
    #  slider set button action
    #
    triedanimation = True

    class setSliderWithButton(Action):

        def __init__(self, direction="", options=None, valuetoadjust=0):
            
            self.direction = direction
            self.options = options
            self.valuetoadjust = valuetoadjust
            self.adjustment = options.get_adjustment()
            self.oldvolume = 0.0
            self.targetvolume = 0.0
            self.tried = "whoopOO"
            

        def __call__(self):

            self.oldvolume=_preferences.get_volume(self.options.mixer)

            if self.direction == "right":

                if (self.adjustment.value < self.adjustment.range):
                    #self.options.selectedIndex = self.options.selectedIndex+1
                    #targetScroll = self.options.selectedIndex*viewPortOptionsPixelWidth
                    volumetoset = _preferences.get_volume(self.options.mixer)+self.valuetoadjust
                    _preferences.set_volume(self.options.mixer,volumetoset)
                    self.tried = False
                    renpy.restart_interaction()
                    self.targetvolume = volumetoset

            if self.direction == "left":

                if (self.adjustment.value > 0):
                    #self.options.selectedIndex = self.options.selectedIndex-1
                    #targetScroll = self.options.selectedIndex*viewPortOptionsPixelWidth
                    volumetoset = _preferences.get_volume(self.options.mixer)-self.valuetoadjust
                    _preferences.set_volume(self.options.mixer,volumetoset)
                    self.tried = False
                    renpy.restart_interaction()
                    self.targetvolume = volumetoset
                    
                    

        def get_sensitive(self):
            if self.direction == "right":
                if self.adjustment.value < self.adjustment.range:
                    return True
                else:
                    return False
            
            if self.direction == "left":
                if self.adjustment.value > 0:
                    return True
                else:
                    return False

        #def periodic(self, st):
            #pronounadjust.change(pronounadjust.get_value()+0.1)
            
                #print st
                # renpy.call_in_new_context(animateBar,self.oldvolume, self.targetvolume)

    


    # TODO: This rules
    # def execute_gametick():
    #     if pronounadjust:
    #         pronounadjust.change(pronounadjust.get_value()+0.25) #only 20hz
    # renpy.config.periodic_callbacks.append(execute_gametick)

    



    #renpy.invoke_in_new_context(animateBar,self.oldvolume, self.targetvolume)
    # def animateBar(oldvolume, targetvolume):
    #     print oldvolume
    #     print targetvolume
    #     renpy.pause(0.1)
    #     animateBar(oldvolume,targetvolume)

            





screen viewportOptions(options,adjust): #TODO: Have individual options be generated by code and animated in with ATL

    button style "minusbutton" action setViewportBox(direction="left",options=options,adjust=adjust,prefKey="test")


    viewport:
        style_prefix "options_viewport"
        xadjustment adjust
        xinitial options.selectedIndex*viewPortOptionsPixelWidth
        #draggable True

        hbox:
            for option in options.desc_list:
                frame:
                    text option
                    

    button style "plusbutton" action setViewportBox(direction="right",options=options,adjust=adjust,prefKey="test")



screen settings:
    tag submenu #use this
    hbox:
        style "settingsHboxLayout"

        vbox:
            style "settingsVboxLayout"
            style_prefix "settings"

            label "Message Speed"
            hbox:
                use viewportOptions(cps_options,cpsadjust)
            label "Player Character Pronoun"
            hbox:
                use viewportOptions(pronoun_options,pronounadjust) 
            label "Dialogue Font"
            hbox:
                use viewportOptions(font_options,fontadjust)
            label "Self-voicing"
            hbox:
                use viewportOptions(selfvoice_options,selfvoiceadjust)

        frame:
            style "seperator"
            pass
            
        vbox:
            style "settingsVboxLayout"
            style_prefix "settings"

            label "SFX Volume"
            hbox:
                button style "minusbutton" action setSliderWithButton(direction="left", options=MixerValue("sfx"), valuetoadjust=0.1)
                bar value MixerValue("sfx")  # TODO: Have a fake slider on top of an animatable bar to animate the values :)
                button style "plusbutton" action setSliderWithButton(direction="right", options=MixerValue("sfx"), valuetoadjust=0.1)
            label "Music Volume"
            hbox:
                button style "minusbutton" action setSliderWithButton(direction="left", options=MixerValue("music"), valuetoadjust=0.1)
                bar value MixerValue("music")
                button style "plusbutton" action setSliderWithButton(direction="right", options=MixerValue("music"), valuetoadjust=0.1)
            label "Field of View"
            hbox:
                bar
  


style settingsHboxLayout:
    xpos 255
    ypos 300
    spacing 60
    ysize 578

style settingsVboxLayout:
    xsize 580
    spacing 11

style seperator:
    yfill True
    xsize 2
    background "#808080"


style settings_label_text:
    color "#F7931E"
    xsize 500
    font gui.name_text_font

style settings_bar:
    left_bar "#ff0000"
    ysize 100

style settings_hbox:
    ysize 80

style settings_slider:
    yalign 0.5
    ysize 25
    left_bar "#AD0BB8"
    right_bar Solid("#B58DAF6E")
    thumb None
    hover_left_bar "#E75FFF"
    hover_right_bar Solid("#B58DAF9C")

style settings_hbox_bar:    
    yalign 0.5

style plusbutton:
    yalign 0.5
    background "gui/art/menu-plus.png"
    hover_background "gui/art/hover_menu-plus.png"
    insensitive_background "gui/art/inactive_menu-plus.png"
    ysize 48
    xsize 48

style minusbutton is plusbutton:
    background "gui/art/menu-minus.png"
    hover_background "gui/art/hover_menu-minus.png"
    insensitive_background "gui/art/inactive_menu-minus.png"

style options_viewport_viewport:
    ysize 50
    yalign 0.5
    

style options_viewport_frame:
    ysize 50
    xsize viewPortOptionsPixelWidth
    background "#8080801F"

style options_viewport_text:
    size 28
    xalign 0.5
    yalign 0.5
    font gui.light_font





        

screen save:
    tag submenu
    #text "savemenu"
    style_prefix "save_load"
    use file_slots(_("Save"))

screen load:
    tag submenu
    #text "othermenu"
    style_prefix "save_load"
    use file_slots(_("Load"))


screen menumain(targetMenu=""): #use targetmenu to SHOW submenus as well as set

    tag menu
    zorder 100

    style_prefix "menumain"
    frame:
        style_prefix "menumain_frame"
        background Image("images/art/menumainbg.png")
        
        if targetMenu != "":
            use expression targetMenu
            use menu_tabs(targetMenu)
        else:
            use save
            use menu_tabs("save")

style return_button:
    ysize 107
    xsize 127
    xalign 0.0
    yalign 0.0
    background "images/art/mainmenu-return-button.png"
    hover_background "images/art/mainmenu-return-button-hover.png"
    yoffset 0

style return_button_fixed:
    xsize 300

style menu_tabs_hbox:
    xoffset 250
    spacing 5
style tab_button:
    padding (55,55,55,25)    
    background "#362934"
    hover_background "#51444F"
    yoffset -9-25+5

style highlighted_tab_button is tab_button:
    yoffset -9-25+10-5+9
    background Frame("images/art/menu_tab_active.png",0,0)
    #hover_background "#fff"

style tab_text:
    color "#fff"
    font gui.name_text_font

style highlighted_tab_text is tab_text:
    color "#272028"
    
style menumain_frame_frame:
    padding (0,0)

    






    