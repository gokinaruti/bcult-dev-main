## Say screen ##################################################################
##
## The say screen is used to display dialogue to the player. It takes two
## parameters, who and what, which are the name of the speaking character and
## the text to be displayed, respectively. (The who parameter can be None if no
## name is given.)
##
## This screen must create a text displayable with id "what", as Ren'Py uses
## this to manage text display. It can also create displayables with id "who"
## and id "window" to apply style properties.
##
## https://www.renpy.org/doc/html/screen_special.html#say

screen say(who, what, namebox_color=None, narrator=False, items=None):


    # if renpy.get_screen("choice"):
    #     style_prefix "pisser"
    #     text "balls"
    # else:
    #     style_prefix "say"
    #     text "ass"

    #Above shit does not work, can do conditionals in displays :)

    style_prefix "say"


    window:
        id "window"

        if renpy.get_screen("choice"):
            #style_prefix "choice" #YO WHY DOES ADDING THIS REMOVE MY VBOX - investigate style_prefix
            #Add custom back button
            imagebutton auto "choice-back-button_%s" action Rollback():
                # at transform:
                #     xoffset 0
                xalign 1.0
                yalign 0.5
                insensitive Image("button_back_insensitive.png")

            #Style for choice
            background Image("gui/art/dialogue-choice.png", xalign=1.0, yalign=1.0)
            xsize 1920
            xpos 1920
            xanchor 1920
            ypos 330

        
        text what id "what":
            if renpy.get_screen("choice"):
                color "#1C1C1C"  

        if renpy.get_screen("choice") is None:
            vbox:
                spacing 8
                at transform:
                    xoffset 1665
                    yoffset -4

                imagebutton auto "button_back_%s" action Rollback():
                    at transform:
                        xoffset 0
                    insensitive Image("button_back_insensitive.png")
                imagebutton auto "button_advance_%s" action RollForward():
                    at transform:
                        xoffset 9
                    insensitive Image("button_advance_insensitive.png")
                imagebutton auto "button_prefs_%s" action ToggleScreen("popup_menu", _transient=True):
                    at transform:
                        xoffset 18



   

    ##NOTE: THIS IS INDENTED WHICH MAKES IT A CHILD OF THE DIALOGUE WINDOW, UNINDENT TO MAKE IT ABSOLUTE POISITONED
        if who is not None:

            add SideImage() xalign 0.0 yalign 0.0 xoffset 214 yoffset 70.5
                
            #TODO: align this dynamically to parent position?

        if who is not None:
            window:
                at namebox
                id "namebox"
                style "namebox"
                text who id "who"
                anchor (0,0)
                ypos -4
                xoffset 0
                if namebox_color is not None:
                        background im.MatrixColor("gui/art/namebox.png",im.matrix.desaturate()*im.matrix.colorize(namebox_color,namebox_color))


transform namebox:
    on replaced:
        xoffset -20
        linear 0.2 xoffset 0

## Say narrate is a custom screen for dilaogue. Can't


## Make the namebox available for styling through the Character object.
init python:
    config.character_id_prefixes.append('namebox')


style window is default

style say_label is default
style say_dialogue is default
style say_thought is say_dialogue

style pisser_dialogue is say_dialogue

style namebox is default
style namebox_label is say_label


style window:
    xalign 0.5
    xfill True
    yalign 0.95
    ysize gui.textbox_height

    background Image("gui/art/dialogue.png", xalign=0.5, yalign=1.0)


    # Note this doesnt work
    # if interact is False:
    #     background Image("gui/art/dialogue-choice.png", xalign=0.5, yalign=1.0)

#style choice_window:


    

style namebox:
    xpos 178
    xanchor 1
    xsize gui.namebox_width
    ypos -4
    ysize gui.namebox_height

    #background Frame("gui/art/namebox.png", gui.namebox_borders, tile=gui.namebox_tile, xalign=gui.name_xalign)
    #padding gui.namebox_borders.padding

    #background Image("gui/art/namebox.png")

    background im.MatrixColor("gui/art/namebox.png",im.matrix.desaturate()*im.matrix.colorize("#2B292D","#2B292D"))
    #TODO: both matrixcolorize from character info

style say_label:
    properties gui.text_properties("name", accent=False)
    xalign 0.5
    yalign 1
    yoffset 10
    xoffset -10


style say_dialogue:
    properties gui.text_properties("dialogue")

    xpos gui.dialogue_xpos
    xsize gui.dialogue_width
    ypos 45
    line_leading 3


style pisser_dialogue:
    kerning 200

style narrator:
    properties gui.text_properties("dialogue")
    xpos 0
    xsize gui.dialogue_width
    ypos 0
    line_leading 3
    kerning 200