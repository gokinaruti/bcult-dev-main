init python:

        # !--------- put the name of the chat here and human readable name --------------!
    chatList.append(  chat1()  )

init -1 python:


    
    def chat1( filled = False ):
       
        chat = Chat( is_filled = filled )

        chat.chat_title="Chat #1"
        chat.chat_description="Introductions"

        #Add this if not (Madoka's Study Group Chat - OSN Messenger)

        #chat.title_bar_text="Your chat title bar text here"



        #--------------------Add messages here-----------------------
        #
        # format:
        #       chat.add_msg( x, y1, y2, y3, ... )
        #           where x = the key of the name of the chatter in chatters.rpy
        #           and y is a kwargs list that takes the following
        #               msg= required
        #               pic=


        chat.add_msg( "jake", msg="hey, if you type in your pw, it will show as stars"  )
        chat.add_msg( "jake", msg=" ********* see!"  )
        chat.add_msg( "goku", msg="hunter2"  )
        chat.add_msg( "goku", msg="doesnt look like stars to me"  )
        chat.add_msg( "jake", msg="Headmaster: *******"  )
        chat.add_msg( "jake", msg="thats what I see"  )
        chat.add_msg( "goku", msg="oh, really?"  )
        chat.add_msg( "jake", msg="Absolutely"  )
        chat.add_msg( "goku", msg="you can go hunter2 my hunter2-ing hunter2!"  )
        chat.add_msg( "goku", msg="haha, does that look funny to you?"  )
        chat.add_msg( "jake", msg="lol, yes. See, when YOU type hunter2, it shows to us as *******"  )
        chat.add_msg( "goku", msg="thats neat, I didnt know OSN did that"  )
        chat.add_msg( "jake", msg="yep, no matter how many times you type hunter2, it will show to us as *******"  )
        chat.add_msg( "goku", msg="awesome!"  )
        chat.add_msg( "goku", msg="wait, how do you know my pw?"  )
        chat.add_msg( "jake", msg="er, I just copy pasted YOUR ******'s and it appears to YOU as hunter2 cause its your pw"  )
        chat.add_msg( "goku", msg="oh, ok."  )



        #=============================================================


        return chat
    



