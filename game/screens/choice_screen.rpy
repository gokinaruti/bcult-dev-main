## Choice screen ###############################################################
##
## This screen is used to display the in-game choices presented by the menu
## statement. The one parameter, items, is a list of objects, each with caption
## and action fields.
##
## https://www.renpy.org/doc/html/screen_special.html#choice

###################################################################
#####To enable chosen boolean
##################################################################
# init python:
#     # Track which choices have been chosen
#     chosen_choice_tracker = None

#     # Action for menu which tracks the chosen choice.
#     class NonPersistentChoice(Action):
#         def __init__(self, label, value, location):
#             self.label = label
#             self.value = value
#             self.location = location

#         def __call__(self):
#             chosen_choice_tracker[(self.location, self.label)] = True
#             return self.value

#     def menu(items):
#         global chosen_choice_tracker
#         if not chosen_choice_tracker:
#             chosen_choice_tracker = {}
#         # Get the name of the current statement
#         # (This is not a public API, but I couldn't find an equivalent.)
#         location = renpy.game.context().current
#         # Build the list of items in the form the screen wants
#         item_actions = [ ]
#         for (label, value) in items:
#             action = NonPersistentChoice(label, value, location)
#             item_actions.append((label, action, (location, label) in chosen_choice_tracker))
#         # Display the screen
#         return renpy.call_screen("choice", items=item_actions)
# #########################################################################

screen choice(items):

#TODO: Probably feed choice attributes to the choice display function and extend the class to have it log per-session info on whats seen or not
#TODO: Do you need to do that? Can't we just do that here with a big long global list of 'chosen' objects

    style_prefix "choice"

    vbox:
        if len(items) is 4:
            spacing 5
            xpos 263
            ypos 347
            yalign 0.0
        #TODO: Adjust say window position if less than 4 so more centered
        for index, i in enumerate(items):            

            frame:
                if i.chosen:
                    style_prefix "chosenthat"
                textbutton i.caption:

                    action i.action

                xoffset index*35
                if len(items) is 4:
                    xoffset index*30
                if i.chosen is False:
                    add "gui/art/choice-arrow.png" size (33,40) yalign 0.5 xoffset 70 yoffset -2
                else:
                    add im.Alpha("gui/art/choice-arrow.png",0.3) size (33,40) yalign 0.5 xoffset 70 yoffset -2
            
            
            
            


## When this is true, menu captions will be spoken by the narrator. When false,
## menu captions will be displayed as empty buttons.
define config.narrator_menu = True


style choice_vbox is vbox
style choice_button is button
style choice_button_text is button_text
style choice_frame is default

style chosenthat_vbox is choice_vbox
style chosenthat_button is choice_button
style chosenthat_button_text is choice_button_text
style chosenthat_frame is choice_frame

style choice_frame:
    xsize 1387
    ysize 168

style choice_vbox:
    xpos 266
    ypos 655
    yanchor 0.5   

    spacing gui.choice_spacing

style choice_button is default:
    properties gui.button_properties("choice_button")
    ysize 200
    background Image("gui/art/choice-box.png")
    
    # margin (0,0)
    # padding (50,50)
    # ysize 925
    # xsize 513
    #xfill True

style choice_button_text is default:
    properties gui.button_text_properties("choice_button")
    color "#EBE6F5"
    yalign 0.5
    xalign 0.0
    xoffset 40
    xsize 1200


style chosenthat_button:
    background im.Alpha("gui/art/choice-box.png",1)

# style chosenthat_button_text:
#     color "#aaa"
#     hover_color "#BCB38E"