﻿# The script of the game goes in this file.



define Pinky = Character("Pinky", image="Pinky", ctc="ourctc", show_namebox_color="#f4008a")
define Headmaster = Character("Headmaster", image="Headmaster", ctc="ourctc", show_namebox_color="#0093d5")
define Pudding = Character("Pudding", image="Pudding", ctc="ourctc", show_namebox_color="#9013c5")
define n = Character(None, show_narrator = True, ctc="ourctc", what_xsize = 1150, what_xpos = 400)

define halftonein = ImageDissolve("images/transitions/halftone-linear.png", 1.5, reverse=True,ramplen=0)

init python:
    def mystery(name=""):
        return Character(name, image="Mystery", ctc="ourctc", callback=whipin)

#For this build: change esc function
default _game_menu_screen = "menumain"

image side Pinky = "character/pinky-side1.png"
image side Headmaster = "character/headmaster-side1.png"
image side Pudding = "character/pudding-side1.png"
image side Mystery = "character/mystery-side1.png"

image Pinky = "pinky default.png"
image Headmaster = "headmaster default.png"
image Pudding = "pudding default.png"

#"#a10b79"

##Wipes out chosen menu choices so greyed out. I can't think of any reason this would be bad as yet
##TODO: Check if this is bad
$renpy.game.persistent._chosen = {}


# The game starts here.




label start:

    ##Wipes out chosen menu choices so greyed out. I can't think of any reason this would be bad as yet
    ##TODO: Check if this is bad
    
    
    $renpy.game.persistent._chosen = {} #FIXME: I think this is bad
    $persistent.chats_seen = []
       

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    

    # This shows a character sprite. A placeholder is used, but you can
    # replace it by aRdding a file named "eileen happy.png" to the images
    # directory.

    # show girl default

    scene bg school


    #show screen Chat(chat1)

    show bg academyday
    with halftonein

    n "Occultar Island. My new home."
   

    n "Out here in the ocean, the view is amazing. Or so I've heard. I'm getting the black-window treatment."

    scene bg black




    mystery("???")"We're approaching the drop point. Are you ready?"
    

    mystery("P")"Close enough."


    call show_interstitial_chat(chat1()) from _call_show_interstitial_chat 

    mystery("???")"You understand your protections and obligations under the Occult Secrets Act?"


    mystery("P")"Yeah, I've done this before."




    mystery("???")"Okay. Good luck in there."

    scene bg academyday




    n "Occultar Academy. The best occult school in the world."
    
    label loopy_menu:
        menu:
            Headmaster "What should I do hey?"
            "bringing the doctors note to game stop saying im allowed to buy as many games as i want and touch myown face if i want to":
                Pinky "Yes?"
                
                jump loopy_menu
            "Shit":
                Pinky "Shit?"
                
                jump loopy_menu
            "Arse":
                Pinky "Arse?"
            "Fake option":
                Headmaster "You dick."
                jump loopy_menu

    menu:
            Headmaster "and why do you think that is?"
            "The":
                pass
            "Mother":
                pass
            "FUCKING SDFSDF":
                pass
                

    n "I'm told the foundation are the \"leading experts in occult science and communication\". Looking at this place, I can believe it."

    n "The Academy only takes students with a lot of \"occult potential\". You have to be talented to get here."

    n "Or lucky, I guess. Or rich."

    show bg black

    n "And technically I was accepted twice. So being expelled wasn't so bad if you think about it."

    Pinky "Well... I'm back."

    show bg black

    Headmaster "Hello, Pinkerton."

    scene bg academyday

    show Pinky at leftc

    Pinky "Oh, uh, hello sir."

    show Headmaster at rightc

    Headmaster "How was the trip? I know we're not easy to get to."

    show screen item("images/tablet-transparent.png",easeinright)
    show screen framebg

    

    n "The Headmaster. Big man in the Foundation. Likes second chances, but I think he liked kicking me out more..."

    hide screen item
    hide screen framebg
    
    Pinky "The helicopter helped. Sir."

    Headmaster "It does that, yes. Speaking of trips, we should talk. Walk with me, please."

    hide Headmaster

    n "It probably wasn't a good idea to ignore a request in the first five minutes..."

    hide Pinky

    n "Or ever, I guess."

    scene bg schoolhall

    n "It was just as I remembered it. Maybe even better."

    n "New clubs were advertising on the walls. The screens were bigger. The wall I tried to put my head through was smooth again."

    n "Class was in session, so our footsteps echoed through the halls. It wasn't due to start until tomorrow. I tried to guess where we were going."

    show Headmaster at rightc

    Headmaster "I know you've been lectured enough already, so I'll make this quick. Do you remember the circumstances leading to your expulsion?"

    show Pinky at leftc

    Pinky "I think so. ...sir."

    Headmaster "I know. This is just a formality. Do you understand that if you get in trouble again, there won't be a third chance?"

    Pinky "I guess. Yeah."

    Headmaster "Fair enough. I believe you, though I would recommend staying away from the sports equipment locker."

    Pinky "You remember that, huh? Heheh..."

    Headmaster "The astronomy club definitely does."

    Pinky "Aheheh..."

    Headmaster "Don't worry, you'll do fine. This is a good opportunity for you."

    Headmaster "One last formality. We need to get you evaluated."

    Pinky "Didn't I do one yesterday?"

    Headmaster "Yes, I know. But you weren't evaluated {i}here{/i}"

    mystery("???")"Don't worry, this one's quick!"

    hide Headmaster
    show Pudding at rightc

    Pudding "Hello! Pinkerton, right?"


    Pinky "Oh, uh, hello... miss?"

    Pudding "You can call me Miss Pudding. I'll be your assessor for today."
    
    Pudding "Tomorrow, I'll be your teacher!"

    hide Pudding
    show Headmaster at rightc

    Headmaster "Ms. Pudding can walk you through it. I have some paperwork to do. We'll talk later."

    hide Headmaster
    show Pudding at rightc

    Pudding "This way, please!"

    hide Pudding
    hide Pinky

    scene bg black

    n "I was dragged in Miss Pudding's wake all the way to her office."





    # These display lines of dialogue.

    

    # This ends the game.

    return
