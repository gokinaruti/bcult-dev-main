init -1 python:

    ctc_fire = False

    def character_talked(x,y,z):
        global ctc_fire
        
        
        if ctc_fire == True:
            ctc_fire = False
            #print "called"
            return None      
        else:
            #print "halting"
            return 0
            
    
    def whipin(event, interact=True, **kwargs):
        global ctc_fire
        if event == "slow_done":
            ctc_fire = True
            #print "end talk event"
        # if event == "show_done":
        #     ctc_fire = False
        if event == "begin":
            ctc_fire = False
        # if event == "show":
        #     ctc_fire = False    


image ourctc:

    "gui/art/eye-dialogue.png"

    yanchor -0.55
    xanchor -0.2
    alpha 0

    block:
        
        function character_talked

        
        
        parallel:
            pause 0.001 #forces initial properties to evaluate properly wrt anchor/transforms when insta displaying
            alpha 0 xanchor 0.9 yanchor -0.9 yzoom 0.2 
            easein_back 0.25 xanchor -0.2 alpha 1.0 yanchor -0.55 yzoom 1
        # parallel:
            
        #     easein_back 0.25 
        parallel:
            xzoom 1.15
            ease 0.125 xzoom 0.85
            easeout 0.125 xzoom 1 

        repeat
  
    


    # OLD
    # xoffset -20 alpha 0 yoffset 10 #yanchor 0.5 yzoom 1 yalign .96
    
                ###
                # pulsing
                ###
                # parallel:
                #     alpha 1.0
                #     ease_quad 0.7 alpha 0.4
                #     ease_quad 0.7 alpha 1.0
                #     pause 0.3
                #     repeat
                # parallel:
                #     rotate 0
                #     easein 0.2 rotate -20
                #     easeout 0.2 rotate 0
                #     easein 0.2 rotate 20
                #     easeout 0.2 rotate 0
                #     repeat

    # block:
    #     function character_talked

    #     parallel:
    #         alpha 0
    #         xoffset -20.0
    #         yoffset 17.0
    #         easein_back 0.2 xoffset 10.0 alpha 1.0 yoffset 10.0
    #     parallel:
    #         yzoom 0.2 
    #         easein_back 0.2 yzoom 1
    #     # parallel:
    #     #     xzoom 1.5
    #     #     easeout 0.1 xzoom 0.5
    #     #     easeout 0.1 xzoom 1 

    #     repeat
    
