init python:

    class Advance_or_drag_check(Action):

        def __init__(self):
            
          pass  
            

        def __call__(self):

            pass

        def get_sensitive(self):
            return False

    chatList = []

    class Chatter:
        # Passed to the chat msgs list
        def __init__(self, codeName=None, displayName=None, colour=None, role=None):
            self._codeName = displayName if codeName==None else codeName
            self.displayName = displayName
            self.colour = colour
            self.role = role

        @property
        def avatar(self):
            return "images/character/"+self._codeName+"-chat.png"
         
    class Chat():
        def __init__(self, chatTitle="", chatDescription="", chatTitleBar="Madoka's Study Group Chat - OSN Messenger", is_filled=False):
            self._msgs = []
            self._display_msgs = []
            self.chat_title = chatTitle
            self.chat_description = chatDescription
            self.title_bar_text = chatTitleBar
            self.is_filled = is_filled

            self.current_displayed = 0
            self.has_been_seen = False

        def add_msg(self, chatter, **kwargs):
            self._msgs.append((chatter,kwargs))

            

        # Queued messages
        @property
        def msgs(self):
            return self._msgs

        # Displayed messages
        @property
        def display_msgs(self):

            if len(self._display_msgs) == 0:
                self._display_msgs.append(self._msgs[0])
                self.current_displayed = 1

            if self.is_filled == True:
                self._display_msgs = self._msgs
            return self._display_msgs

        def push_next(self):
            self._display_msgs.append(self._msgs[self.current_displayed])
            self.current_displayed += 1


        # @msgs.setter
        # def msgs(self,value):
        #     self._msgs = value


#------
# Screens
#------

init python:
    temp = 0.0

screen Chat(chat):

    

    style_prefix "Chat"


    
    frame:
        ysize 950
        
        vbox:
            ysize 200

            fixed:
                style "title_bar"
                
                add "images/art/chat_title_bar.png"

                button style "close_button" action Return()

                text chat.title_bar_text
            
            hbox:
                #ysize 950-100
                use Chat_scroll(chat)
                $print "Value:"+str(YScrollValue("chat").get_adjustment().value)
                $print "Range:"+str(YScrollValue("chat").get_adjustment().range)
                if YScrollValue("chat").get_adjustment().range > 1 or chat.is_filled == True:
                    vbar value YScrollValue("chat")
                else:                
                    frame:
                        background Solid("#272028")
                        xsize 9
        
        #button style "full_covering_button" action Advance_or_drag_check()


style full_covering_button:
    background "#ffffff55"
    xfill True
    yfill True
    ysize 500


        

screen Chat_scroll(chat):

    style_prefix "Chat_scroll"
    $chatscroll = ui.adjustment()
    

    viewport id "chat":
            draggable True
            mousewheel True
            yadjustment chatscroll
            style_prefix "Chat_line"
            
            $chatscroll.value = float("inf")
            frame:
                has vbox 
                
                # Calls function_name() where function_name = chatName then accesses its msgs property which is a populated Chat() object
                #for chatter, kwargs in globals()[chatName]().msgs:.
                #TODO: Can we use showif (https://www.renpy.org/doc/html/screens.html#showif-statement) to use on show ATL statements on them?
                for chatter, kwargs in chat.display_msgs:
                    
                    use Chat_line(chatters[chatter],**kwargs)

screen Chat_line(chatter,**kwargs):
    
    hbox:
        style "Chat_line_hbox"
        style_prefix "Chat_line_hbox"
        add chatter.avatar
        vbox:
            
            hbox:
                style "Chat_line_name"
                style_prefix "Chat_line_name"
                if chatter.colour:
                    text chatter.displayName color chatter.colour
                else:
                    text chatter.displayName
                if chatter.role:
                    text chatter.role style "Chat_user_role"
            frame:
                style "chat_msg_frame"
                text kwargs["msg"]

screen Interstitial_chat(chat):
    fixed:
        
        window:
            style "interstitial_chat"
            use Chat(chat)

            
    textbutton "Back" action Rollback()


    

label show_interstitial_chat(calledchat, final=False):

    window hide
    
    #TODO: Make the initial screen during rollbacks?

    show screen Interstitial_chat( calledchat )
    
    python:
        
        if calledchat.chat_title in persistent.chats_seen:
            calledchat.is_filled = True
            renpy.call_screen("Continue_button",final)
            #renpy.pause(hard=True)
        else:
            for msgs in calledchat.msgs:
                if calledchat.current_displayed < len(calledchat.msgs):
                    calledchat.push_next()
                renpy.pause()
              
    if calledchat.current_displayed == len(calledchat.msgs):
        call screen Continue_button(final)
        #$renpy.pause(hard=True)
        
        python:
            if persistent.chats_seen:
                persistent.chats_seen.append(calledchat.chat_title)
            else:
                persistent.chats_seen = []
                persistent.chats_seen.append(calledchat.chat_title)
                

    hide screen Interstitial_chat
    hide screen Continue_button
    return

screen Continue_button(final=False):

    if final == True:
        textbutton "Save" action Return():
            yoffset 70

    textbutton "Continue" action Return():
        yoffset 50

style Chat_line_text:
    color "#ffffff"
    size 18
    #outlines [ (1, "#ff00ff", absolute(0), absolute(0)) ]

style Chat_frame:
    xsize 780
    padding (0,0)
    background "#2E2B33"

style Chat_hbox:
    xsize 780-9
    ysize 850
    spacing 0

style Chat_vbox:
    ysize 500

style Chat_scroll_viewport:
    yfill True
    xfill True

style Chat_vscrollbar:
    xsize 9
    thumb Frame("images/art/chat_[prefix_]scroll_bar.png", gui.vscrollbar_borders, tile=gui.scrollbar_tile)
    base_bar Solid("#272028")

# style Chat_scroll_frame:
#     #xsize 780
#     background None
#     padding (20,20)

style Chat_line_frame:
    background None
    padding (32,32)

style Chat_line_vbox:
    xfill True
    spacing 45

style Chat_line_hbox:
    spacing 35

style Chat_line_hbox_vbox:
    spacing 15

style chat_msg_frame:
    padding (35,0,0,10)
    #background "images/art/chat-vertical-line.png"
    background Frame("images/art/chat-frame-background.png", 5,5)

style Chat_line_name:
    xoffset 35
    spacing 10

style Chat_line_hbox_text: #chat msgs
    size 23
    font gui.light_font
    yoffset -3
    hinting "auto"

style Chat_line_name_text: #username
    size 24
    

style Chat_user_role: #role
    size 21
    font gui.light_font
    yalign 1.0
    yoffset -2
    color "#666666"


style Chat_scroll_vscrollbar_image:
    thumb "images/art/chat_scroll_bar.png"

style title_bar:
    color "#ffffff"
    ysize 100

style Chat_text: #title bar
    color "#919191"
    size 19
    yoffset 15
    xoffset 63


style close_button:
    xalign 1.0
    yalign 0.0
    xsize 47
    ysize 43
    background "images/art/chat_close_button.png"



style interstitial_chat:
    yalign 0.5
    xalign 0.5
    xfill False
    yfill False
    #Iypos 50
    #xpos 1920/2
    #yoffset 30