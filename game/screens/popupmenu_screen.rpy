screen popup_menu():
    style_prefix "popup_menu"
    frame:
        vbox:
            textbutton _("Back") action Rollback()
            textbutton _("History") action ShowMenu('history')
            textbutton _("Skip") action Skip() alternate Skip(fast=True, confirm=True)
            textbutton _("Auto") action Preference("auto-forward", "toggle")
            textbutton _("Save") action ShowMenu('save')
            textbutton _("Q.Save") action QuickSave()
            textbutton _("Q.Load") action QuickLoad()
            textbutton Settings action ShowMenu('menumain','settings')


style popup_menu_frame:
    background "#000000cc"
    xfill False
    yfill False
    xalign 1.0
    yalign 0.5
    ysize None
    xoffset -20
    padding (20,20)